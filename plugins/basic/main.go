package basic

import (
	"log"

	"../../vkapi"
	"github.com/jmoiron/sqlx"
	"github.com/valyala/fastjson"
)

// Plugin описывает плагин
type Plugin struct {
	Cmds   []string
	Desc   string
	Data   *fastjson.Value
	VK     *vkapi.VK
	DB     *sqlx.DB
	Role   uint8
	Values struct {
		Args   []string
		PeerID int64
		FromID int64
	}
}

// APIError описывает ошибку API
type APIError struct {
	Service string
	Method  string
	Code    int
}

// ErrorNoArguments описывает ошибку недостатка аргументов
var ErrorNoArguments = APIError{
	Service: "bot",
	Method:  "no_enough_arguments",
	Code:    1,
}

// ErrorPermissionDenied описывает ошибку о недостатке прав
var ErrorPermissionDenied = APIError{
	Service: "bot",
	Method:  "permission_denied",
	Code:    1,
}

// ErrorDBError описывает ошибку взаимодействия с базой данный
var ErrorDBError = APIError{
	Service: "bot",
	Method:  "db_error",
	Code:    1,
}

// GenerateAPIError генерирует basic.APIError из JSON-представления
func GenerateAPIError(resp []byte) APIError {
	var p fastjson.Parser

	json, err1 := p.Parse(string(resp))
	Check(err1)

	if json.Exists("error") {
		err := json.Get("error")
		var apierror APIError

		for _, v := range err.GetArray("request_params") {
			if string(v.GetStringBytes("key")) == "method" {
				apierror.Method = string(v.GetStringBytes("value"))
			}
		}

		apierror.Service = "vk"
		apierror.Code = err.GetInt("error_code")

		return apierror
	}

	return APIError{}
}

// Check обработка исключений
func Check(err error, die ...bool) {
	if err != nil {
		if len(die) == 1 {
			log.Fatalln(err)
		} else {
			log.Println("Некритичная ошибка:", err)
		}
	}
}

// CanExec проверяет, может ли пользователь исполнить команду
func (p *Plugin) CanExec(peer_id, user_id int64) bool {
	if p.Role == 0 {
		return true
	}

	stmt, err := p.DB.Preparex(p.DB.Rebind("SELECT role FROM role_assign WHERE role>=? AND user_id=? AND peer_id=?"))
	Check(err)
	if err != nil {
		return false
	}

	rows, err := stmt.Queryx(p.Role, p.Values.FromID, p.Values.PeerID)
	Check(err)
	if err != nil {
		return false
	}

	if rows.Next() {
		return true
	}

	if p.VK.BotOwner == user_id {
		return true
	}

	return false
}
