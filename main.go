package main

import (
	"log"

	"./errorapi"
	"./plugins/basic"
	"./utils"
	"./vkapi"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// обработка исключений
func _check(err error, die ...bool) {
	if err != nil {
		if len(die) == 1 {
			log.Fatalln(err)
		} else {
			log.Println("Некритичная ошибка:", err)
		}
	}
}

func main() {
	log.Println(":: Запускаю бота...")
	var vk vkapi.VK
	cfg := parseConfig("./config.json")
	vk.Init(cfg.Token, cfg.GroupID, cfg.BotOwner)

	var PSys Plugins
	PSys.Load(cfg.Plugins)

	log.Println(":: Подключение к БД")
	conn, err := sqlx.Connect("mysql", cfg.DB.Username+":"+cfg.DB.Password+"@/"+cfg.DB.DBName) // "user:password@/dbname"
	_check(err, true)

	var ErrorHandler errorapi.ErrorHandler
	ErrorHandler.Init(&vk, cfg.ErrorDictionary)

	for {
		Handle(&vk, &PSys, &ErrorHandler, conn)
	}
}

// Handle крутится в бесконечном цикле и получает новые сообщения
func Handle(vk *vkapi.VK, p *Plugins, e *errorapi.ErrorHandler, sql *sqlx.DB) {
	upd := vk.ListenLongPoll()
	for _, v := range upd.Updates {
		if v.Type == "message_new" {
			sym, found := p.Sym(utils.ExtractCmd(string(v.Object.GetStringBytes("text"))))
			if found {
				sym.Data.VK = vk
				sym.Data.DB = sql
				sym.Data.Data = v.Object
				if sym.Data.CanExec(v.Object.GetInt64("peer_id"), v.Object.GetInt64("from_id")) {
					go func() {
						err := sym.Launch()
						e.Handle(&sym.Data, &err)
					}()
				} else {
					sym.Data.Values.PeerID = v.Object.GetInt64("peer_id")
					e.Handle(&sym.Data, &basic.ErrorPermissionDenied)
				}
			}
		}
	}
}
